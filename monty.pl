#!/usr/bin/env perl

use strict;
use warnings;
use 5.10.0;

# NOTE:  we got the thing about the goat being the prize backwards
#        from how this is usually defined.

# basic setup:
# you get 3 doors
# - initially, you pick a door at random
# - second, the host eliminates one of the two remaining doors that you haven't picked
# - now, you have the option to switch to the remaining door from the one you've already picked

my ($switched, $unswitched) = (0, 0);
my $doors = 3;

for (my $i = 0; $i < 1000000; $i++) {

  my $goat = int(rand($doors));
  my $initial_selection = int(rand($doors));

  # my $host_eliminates = int(rand($doors));
  # until (($host_eliminates != $initial_selection) && ($host_eliminates != $goat)) {
  #   $host_eliminates = int(rand($doors));
  # }

  # THIS IS OUR BUG (i kind of think)
  # my $final_selection = int(rand($doors));
  # until ($final_selection != $host_eliminates) {
  #   $final_selection = int(rand($doors));
  # }

  if ($goat == $initial_selection) {
    $unswitched++;
  } else {
    $switched++;
  }

  # say "you pick: $initial_selection"; say "host eliminates: $host_eliminates";
  # say "goat is: $goat";
  # say "final selection is: $final_selection";
  # say "correct? " . ($result ? 'yes' : 'no');

  if ($i % 10000 == 0) {
    say "$switched, $unswitched ";
  }

}

say "switched from initial decision: $switched";
say "unswitched from initial decision: $unswitched";

