use strict;

my $iterations = 3000;    # How many games to play
my $verbosity = 0;


while (@ARGV) {

   my $param = shift @ARGV;
   $verbosity = 1 if $param eq '-v';
   $iterations = int (shift @ARGV) if $param eq '-i';
}

sub verbose {

   print $_[0]."\\n" if $verbosity;
}

my $stickers;
my $switchers;


print "Playing $iterations games...\\n\\n";


for(1..$iterations) {

   my @items = qw(goat goat prize);     # two goats, one prize
   my @door;

   while (@items) {            # this puts the @items into the @door array in random order
       push (@door, splice (@items, int rand @items, 1));
   }


   verbose ("Door 0: $door[0];  Door 1: $door[1];  Door 2: $door[2]");


   my $contestant = int rand 3;
   verbose ("Contestant chooses door $contestant.");


   my $monty;


   # If the contestant picked the prize, Monty picks another door by random.
   if ($door[$contestant] eq 'prize') {
       $monty = ($contestant + (int rand 2) + 1) % 3;
   }


   # Otherwise, he picks the other goat.
   else {
       $monty = $door [ ($contestant+1) % 3 ] eq 'goat' ? ($contestant+1) % 3 : ($contestant+2) % 3;
   }


   verbose ("Monty opens door $monty.");


   # Now only two doors are open. If the sticker wins, the switcher loses and vice versa.
   if ($door[$contestant] eq 'prize') {
       verbose ("Sticker wins.  Switcher loses.");
       $stickers++;
   } else {
       verbose ("Sticker loses.  Switcher wins.");
       $switchers++;
   }

}

print "Grand totals:
Sticker  has won  $stickers  times
Switcher has won  $switchers  times
";
